# Home Goods Warehouse GBM

This project was developed in microframework flask for python. It is an example
of a home goods warehouse, you can create, read, update and delete products.

The api and frontend are deployed in App Engine and Cloud Storage of GCP, 
you can check all the requests into postman. You can find all the 
posible requests in the next pusbished Postmant doc:

* [Postman Doc](https://documenter.getpostman.com/view/4921800/RWgxvbAE) - Postman requests(CRUD)
* [Web Site](http://warehousegbm.s3-website-us-east-1.amazonaws.com) - Here you can check the frontent project (Only works get products and add products)  

* The We site 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Python 3.0 or latest

Install the virtualenv tool with pip

Download or Clone the project: 
```

### Installing

Clone the repository

```
git clone https://pablomt@bitbucket.org/pablomt/warehousegbm.git
```

Create virtualenv

```
virtualenv -p python3 venv
```

Create virtualenv

```
virtualenv -p python3 venv
```

Activate virtualenv

```
source venv/bin/activate
```

Install requirements

```
pip install -r requirements.txt
```

Run the app

```
 python manage.py
```

Then you can open [Postman](https://documenter.getpostman.com/view/4921800/RWgxvbAE) and make requests to localhost


## Deployment

Use Google Cloud SDK for mac.
* Create de app: ```gcloud app create```
* Deploy the app: ``` gcloud app deploy```

## Built With

* [Flask](http://flask.pocoo.org) - Microframework python
* [MLab](https://mlab.com) - Used mLab for MongoDB 
* [GCP](https://console.cloud.google.com/) - The cloud solution for deploying the api
* [AWS](https://aws.amazon.com/console/) - The cloud solution where is hosting the Static Website    


## Authors

* **Pablo Moreno Tepichín**

## License

This project is licensed under the MIT License

## Acknowledgments

GBM Assessment

