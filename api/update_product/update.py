from db.dbhelper import DBHelper
from datetime import datetime

'''
    Function for uodate product
'''


def update_product(key, product_name, description, price, quantity):
    # instance database
    db = DBHelper()
    # Obtain the current day to update it into database in modified field
    modified_at = int(datetime.today().timestamp())
    query = {'$and': [
        {'key': {'$ne': key}},
        {'name': product_name}
    ]}
    fields = {'name': 1, 'description': 1, 'price': 1, 'updated_at': 1, 'quantity': 1}
    check_duplicate_name = db.check_duplicate_name(query, fields)
    if check_duplicate_name:
        response = 'Ya existe ese nombre de producto en el sistema'
    else:
        # Create into database the document with the product's information
        response = db.update_values(key, (
            {'name': product_name, 'description': description, 'price': price, 'quantity': quantity,
             'updated_at': modified_at}))
    return {'name': product_name, "message": response}
