from flask.views import MethodView
from flask import jsonify, request
from update_product.update import update_product
import logging


# Endpoint to create products
class UpdateProductAPI(MethodView):
    logger = logging.getLogger(__name__)

    def get(self):
        return jsonify({'upload-api': 'This api is for update general info for specific product'}), 200

    def post(self):
        data = request.json
        self.logger.info(data)

        # Obtain all values to create new product
        key = data.get('key')
        product_name = data.get('product_name')
        description = data.get('description')
        price = data.get('price')
        quantity = data.get('quantity')
        try:
            response = update_product(key, product_name, description, price, quantity)
        except Exception as exception_message:
            # Check if the name of product already exists in db
            if 'duplicate key error index' in str(exception_message):
                response = "El producto que deseas agregar con ese nombre ya existe en el sistema"
            else:
                response = str(exception_message)
        return jsonify({
            'response': response
        }), 201
