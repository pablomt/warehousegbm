from flask import Blueprint
from update_product.api import UpdateProductAPI

# View to create new product
update_product_app = Blueprint('update_product_app', __name__)
update_product_view = UpdateProductAPI.as_view('update_product_app')

# Add URLS for create product api to both methods POST and GET
update_product_app.add_url_rule('/update_product', view_func=update_product_view, methods=['POST', ])
update_product_app.add_url_rule('/update_product', view_func=update_product_view, methods=['GET', ])
