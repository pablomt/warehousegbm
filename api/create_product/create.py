import config
from db.dbhelper import DBHelper
import os
from datetime import datetime

'''
    Function for create new product
'''


def create_product(product_name, description, price, quantity):
    # instance database
    db = DBHelper()
    # Obtain the current day to save it into database
    created_at = int(datetime.today().timestamp())
    modified_at = int(datetime.today().timestamp())
    # Create into database the document with the product's information
    identifier = db.create_product(product_name, description, price, quantity, created_at, modified_at)
    return {'id': identifier, 'name': product_name}
