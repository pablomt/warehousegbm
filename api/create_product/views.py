from flask import Blueprint
from create_product.api import CreateProductAPI

# View to create new product
create_product = Blueprint('create_product', __name__)
create_product_view = CreateProductAPI.as_view('create_product')

# Add URLS for create product api to both methods POST and GET
create_product.add_url_rule('/create_product', view_func=create_product_view, methods=['POST', ])
create_product.add_url_rule('/create_product', view_func=create_product_view, methods=['GET', ])
