from flask import Blueprint
from get_products.api import GetListProductsAPI

get_products = Blueprint('get_products', __name__)

get_products_api = GetListProductsAPI.as_view('get_products')

get_products.add_url_rule('/products', view_func=get_products_api, methods=['POST', ])
get_products.add_url_rule('/products', view_func=get_products_api, methods=['GET', ])
