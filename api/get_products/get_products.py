from db.dbhelper import DBHelper

'''
Function to obtain all the products, the function receives sort_data(quantity, price or alphabet)
'''


def get_products(sort_data):
    db = DBHelper()
    products = db.get_products({}, sort_data)
    return products
