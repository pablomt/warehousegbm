from flask.views import MethodView
from flask import jsonify, request
import logging
from get_products.get_products import get_products

'''
Api to get all the products, receives: {"sort_data": "alphabet || quantity || price" }
'''


class GetListProductsAPI(MethodView):
    logger = logging.getLogger(__name__)

    def get(self):
        return jsonify({'upload-api': 'get list of products api'}), 200

    def post(self):
        data = request.json
        self.logger.info(data)
        sort_data = data.get('sort_data')

        response = get_products(sort_data)

        return jsonify(response), 201
