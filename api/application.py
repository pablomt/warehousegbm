import logging
from flask import Flask
from flask_cors import CORS


def create_app(config, debug=False, testing=False, config_overrides=None):
    """
    Create all the API configurations, this handle the database connection, the routes
    initialization, the SSL certificate and all the error handlers

    Arguments
        name: config,             type: module,         summary: This file is in charge of the database connections and credentials
        name: debug,              type: boolean,        summary: It set the environment as development phase or not
        name: testing,            type: boolean,        summary: It set the environment as testing
        name: config_overrides,   type: boolean,        summary: This will say if the initial config is other rather the initial one

    Call
        create_app('module', True, False, False)

    Response
        None
    """

    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}})
    app.config.from_object(config)

    # Initialize enviroment
    app.debug = debug
    app.testing = testing

    # Apply configurations overrides
    if config_overrides:
        app.config.update(config_overrides)

    # Initialize logs
    if not app.testing:
        logging.basicConfig(level=logging.INFO)

    # Import blueprints
    from create_product.views import create_product
    from get_products.views import get_products
    from search_engine.views import search_product_app
    from update_product.views import update_product_app
    from delete_product.views import delete_product_app
    from supply_product.views import supply_product_app

    # Register blueprint
    app.register_blueprint(create_product, url_prefix='/api/v1')
    app.register_blueprint(get_products, url_prefix='/api/v1')
    app.register_blueprint(search_product_app, url_prefix='/api/v1')
    app.register_blueprint(update_product_app, url_prefix='/api/v1')
    app.register_blueprint(delete_product_app, url_prefix='/api/v1')
    app.register_blueprint(supply_product_app, url_prefix='/api/v1')

    return app
