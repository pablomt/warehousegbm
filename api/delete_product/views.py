from flask import Blueprint
from delete_product.api import DeleteProductAPI

# View to create new product
delete_product_app = Blueprint('delete_product_app', __name__)
delete_product_view = DeleteProductAPI.as_view('delete_product_app')

# Add URLS for create product api to both methods POST and GET
delete_product_app.add_url_rule('/delete_product', view_func=delete_product_view, methods=['POST', ])
delete_product_app.add_url_rule('/delete_product', view_func=delete_product_view, methods=['GET', ])
