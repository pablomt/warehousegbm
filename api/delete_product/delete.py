from db.dbhelper import DBHelper

'''
    Function for delete a product
'''


def delete_product(product_key):
    db = DBHelper()
    response = db.delete_product({'key': product_key})
    return response
