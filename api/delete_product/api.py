from flask.views import MethodView
from flask import jsonify, request
from delete_product.delete import delete_product
import logging


# Endpoint to create products
class DeleteProductAPI(MethodView):
    logger = logging.getLogger(__name__)

    def get(self):
        return jsonify({'upload-api': 'This api is for delete specific product'}), 200

    def post(self):
        data = request.json
        self.logger.info(data)
        product_key = data.get('product_key')

        response = delete_product(product_key)

        return jsonify(response), 201
