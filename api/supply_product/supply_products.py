from db.dbhelper import DBHelper
from datetime import datetime

'''
    Function for supply products to customers
'''


def supply_product(key, product_name, quantity_product_out):
    # instance database
    db = DBHelper()
    # Obtain the current day to update it into database in modified field
    updated_at = int(datetime.today().timestamp())
    query = {'name': product_name}
    fields = {'_id': 0, 'key': 1, 'name': 1, 'description': 1, 'quantity': 1, 'price': 1}
    product_search = db.search_product(query, fields)
    # Check if you have enough stock in warehouse
    if int(quantity_product_out) > product_search['quantity']:
        response = "No cuentas con el stock suficiente. tu stock es de " + str(product_search['quantity'])
    # Check that the quantity out is greater than 0
    elif int(quantity_product_out) <= 0:
        response = "No es posible abastecer cantidades negativas o en cero"
    else:
        total = product_search['price'] * int(quantity_product_out)
        new_stock = product_search['quantity'] - int(quantity_product_out)
        response = db.supply_product(key, (
            {'previous_quantity': product_search['quantity'], 'quantity_product_out': quantity_product_out, 'total'
             : total, 'supply_date': updated_at}), new_stock, updated_at)

    return {'name': product_name, "message": response}
