from flask import Blueprint
from supply_product.api import SupplyProductAPI

# View to create new product
supply_product_app = Blueprint('supply_product_app', __name__)
supply_product_view = SupplyProductAPI.as_view('supply_product_app')

# Add URLS for create product api to both methods POST and GET
supply_product_app.add_url_rule('/supply_product', view_func=supply_product_view, methods=['POST', ])
supply_product_app.add_url_rule('/supply_product', view_func=supply_product_view, methods=['GET', ])
