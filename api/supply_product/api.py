from flask.views import MethodView
from flask import jsonify, request
from supply_product.supply_products import supply_product
import logging


# Endpoint to create products
class SupplyProductAPI(MethodView):
    logger = logging.getLogger(__name__)

    def get(self):
        return jsonify({'upload-api': 'This api is for supply product to a customer'}), 200

    def post(self):
        data = request.json
        self.logger.info(data)

        # Obtain all values to create new product
        key = data.get('key')
        product_name = data.get('product_name')
        quantity_product_out = data.get('quantity_product_out')
        response = supply_product(key, product_name, quantity_product_out)
        return jsonify({
            'response': response
        }), 201
