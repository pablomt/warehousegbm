from flask import Blueprint
from search_engine.api import SearchProductAPI

search_product_app = Blueprint('search_product_app', __name__)

search_product_api = SearchProductAPI.as_view('search_product_app')

search_product_app.add_url_rule('/product', view_func=search_product_api, methods=['POST', ])
search_product_app.add_url_rule('/product', view_func=search_product_api, methods=['GET', ])
