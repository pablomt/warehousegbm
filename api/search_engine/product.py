from db.dbhelper import DBHelper

'''
Function to return specific product searching by name
'''


def search_product(key):
    db = DBHelper()
    response = db.search_product({'key': key},
                                 {'_id': 0, 'key': 1, 'name': 1, 'description': 1, 'quantity': 1, 'price': 1,
                                  'supply': 1})
    return response
