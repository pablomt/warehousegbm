from flask.views import MethodView
from flask import jsonify, request
import logging
from search_engine.product import search_product

'''
Api to get all the products, receives: {"sort_data": "alphabet || quantity || price" }
'''


class SearchProductAPI(MethodView):
    logger = logging.getLogger(__name__)

    def get(self):
        return jsonify({'upload-api': 'API for search specific product by name'}), 200

    def post(self):
        data = request.json
        self.logger.info(data)
        key = data.get('key')

        response = search_product(key)

        return jsonify(response), 201
