from pymongo import MongoClient, ASCENDING, DESCENDING
from bson.objectid import ObjectId
from config import DB_URI
from datetime import datetime


class DBHelper:
    def __init__(self):
        client = MongoClient(DB_URI,
                             connectTimeoutMS=30000,
                             socketTimeoutMS=None,
                             socketKeepAlive=True)
        db = client['warehousegbm']
        self.collection = db.products

    # Create the product into the mongoDB
    def create_product(self, product_name, description, price, quantity, created_at, updated_at):
        identifier = ObjectId()
        self.collection.insert(
            {'_id': identifier, 'key': str(identifier), 'name': product_name, 'description': description,
             'price': price, 'quantity': quantity, 'created_at': created_at, 'updated_at': updated_at})
        return str(identifier)

    # Get products order by alphabet, quantity or price
    def get_products(self, query, sort_data):
        if sort_data == 'price':
            sort_data = {'price': -1}
        elif sort_data == 'quantity':
            sort_data = {'quantity': -1}
        else:
            sort_data = {'name': 1}

        response = list(self.collection.aggregate([
            {'$match': query},
            {'$group':
                {
                    '_id':
                        {
                            'key': "$key",
                            'name': "$name",
                            'quantity': "$quantity",
                            'price': "$price",
                            'created_at': "$created_at",
                            'updated_at': "$updated_at",
                            'description': "$description",
                            'supply': "$supply"
                        },
                }
            },
            {'$project':
                {
                    '_id': 0,
                    'key': "$_id.key",
                    'name': "$_id.name",
                    'quantity': "$_id.quantity",
                    'price': "$_id.price",
                    'created_at': "$_id.created_at",
                    'updated_at': "$_id.updated_at",
                    'description': "$_id.description",
                    'supply': "$_id.supply"
                }
            },
            {'$sort': sort_data}
        ]))
        # Convert timestamp to date
        for product in response:
            product['created_at'] = datetime.fromtimestamp(product['created_at']).strftime('%Y-%m-%d %H:%M:%S')
            product['updated_at'] = datetime.fromtimestamp(product['updated_at']).strftime('%Y-%m-%d %H:%M:%S')
            if 'supply' in product:
                for supply in product['supply']:
                    supply['supply_date'] = datetime.fromtimestamp(supply['supply_date']).strftime('%Y-%m-%d %H:%M:%S')
        return response

    # Search product by name
    def search_product(self, query, fields):
        response = self.collection.find_one(query, fields)
        return response

    # Update values of product, recieve the identifier(name) and the values(s) want to update
    def update_values(self, key, values):
        self.collection.update({'key': key}, {'$set': values})
        # print(str(self.collection.update({'key': key}, {'$set': values})))
        return 'Producto Actualizado'

    # This function is to prevent change the name of a prodcut and assign one that already exists
    def check_duplicate_name(self, query, fields):
        response = self.collection.find_one(query, fields)
        return response

    # Function to a delete product
    def delete_product(self, query):
        response = self.collection.remove(query)['n']
        if response == 0:
            response = {'response': 'Error al borrar'}
        else:
            response = {'response': "Producto eliminado con exito"}
        return response

    # Function to supply product to customer, this function adds another array into supply field
    def supply_product(self, key, values, new_stock, updated_at):
        self.collection.update({'key': key}, {'$set':
            {
                'quantity': new_stock,
                'updated_at': updated_at,
            },
            '$addToSet': {'supply': values}
        })
        return 'Se ha abastecido al proveedor y descontado el stock de tu almacén'
