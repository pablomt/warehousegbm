/* jshint esversion: 6 */
/* jshint -W097 */ // Ignore 'use strict' on each function
/* jshint -W117 */ // Ignore 'use strict' on each function
"use strict";

/**
 * @global
 * @desc GCP API Address
 */
const WAREHOUSE_API_URL = "https://gbmwarehouse-220602.appspot.com";
// const WAREHOUSE_API_URL = "http://localhost:5000";

/**
 * @global
 * @desc get all product
 */
const ALL_PRODUCTS_ENDPOINT = "/api/v1/products";


/**
 * @global
 * @desc Endpoint to add product
 */
const NEW_PRODUCT_ENDPOINT = "/api/v1/create_product";

// /**
//  * @global
//  * @desc Endpoint to search a product
//  */
// const SEARCH_PRODUCT_ENDPOINT ="/api/v1/product";
//
// /**
//  * @global
//  * @desc Endpoint to dele a product
//  */
// const DELETE_PRODUCT_ENDPOINT ="/api/v1/delete_product";
//
// /**
//  * @global
//  * @desc Endpoint to update a product
//  */
// const UPDATE_PRODUCT_ENDPOINT ="/api/v1/update_product";
//
//
// /**
//  * @global
//  * @desc Endpoint to supply products to a client
//  */
// const SUPPLY_PRODUCT_ENDPOINT ="/api/v1/supply_product";
