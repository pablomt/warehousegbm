var prodcuts_arr = [];

var loop_break = false;

var settings = {
  "async": true,
  "crossDomain": true,
  "url": WAREHOUSE_API_URL + ALL_PRODUCTS_ENDPOINT,
  "method": "POST",
  "headers": {
    "content-type": "application/json",
    "cache-control": "no-cache",
    "postman-token": "fcdaa395-03e8-399a-6af6-7b8db90918dd"
  },
  "processData": false,
  "data": "{\n\t\"sort_data\": \"quantity\"\n}"
}

$.ajax(settings).done(function (response) {
  product_arr = response;
  while(!loop_break) {
    // Everything loaded
    if (product_arr) {
      loop_break = true;

      $('.loader').addClass('loader-hidden');
      $('.loader-hidden').addClass('display-none');
    }
  }
  // console.log(product_arr);
  add_to_table(product_arr);
});

function add_to_table(data) {
  // EXTRACT VALUE FOR HTML HEADER.
  var col = [];
  for (var i = 0; i < data.length; i++) {
    for (var key in data[i]) {
      if (col.indexOf(key) === -1) {
        col.push(key);
      }
    }
  }
  var table = document.getElementById("table-products"); //give this ID to your table

  // ADD JSON DATA TO THE TABLE AS ROWS.
  for (var i = 0; i < data.length; i++) {
    tr = table.insertRow(-1);
    tr.insertCell(0).innerHTML = data[i].key;
    tr.insertCell(-1).innerHTML = data[i].name;
    tr.insertCell(2).innerHTML = data[i].description;
    tr.insertCell(3).innerHTML = data[i].price;
    tr.insertCell(4).innerHTML = data[i].quantity;
    tr.insertCell(5).innerHTML = data[i].created_at;
    tr.insertCell(6).innerHTML = data[i].updated_at;
    var a = document.createElement('a');
    var btn = document.createElement("BUTTON");
    var t = document.createTextNode("CLICK ME");
    //a.href = '#editProductModal';
    tr.insertCell(7).innerHTML = '<a href="#editProductModal" class="edit" \
    data-toggle="modal"><i class="material-icons" data-toggle="tooltip" \
    title="Edit">&#xE254;</i></a> <a href="#deleteProductModal" class="delete" \
    data-toggle="modal"><i class="material-icons" data-toggle="tooltip"\
    title="Delete">&#xE872;</i></a>';
  }
}

function create_product() {
  var product_name = document.getElementById("formCreateProduct").elements.namedItem("product_name").value;
  var description = document.getElementById("formCreateProduct").elements.namedItem("description").value;
  var price = document.getElementById("formCreateProduct").elements.namedItem("price").value;
  var quantity = document.getElementById("formCreateProduct").elements.namedItem("quantity").value;
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": WAREHOUSE_API_URL + NEW_PRODUCT_ENDPOINT,
    "method": "POST",
    "headers": {
      "content-type": "application/json",
      "cache-control": "no-cache",
      "postman-token": "fcdaa395-03e8-399a-6af6-7b8db90918dd"
    },
    "processData": false,
    "data": "{\n\t\"product_name\": \""+product_name+"\", \n\t\"description\": \""+description+"\", \n\t\"price\": \""+price+"\", \n\t\"quantity\": \""+quantity+"\"\n}"
  }

  $.ajax(settings).done(function (response) {
    alert(response)
    console.log(response)
    if (response.response == "El producto que deseas agregar ya existe en el sistema") {
        alert("El producto que deseas agregar ya existe en el sistema, favor de verificar los datos")
    }
    else {
    alert("Producto agregado exitosamente!");
    }
  });
}
